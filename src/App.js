// import Navbar from "./components/Navbar";
import Home from "./components/Home";
import Create from "./components/Create";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from "./components/Login";
import SignUp from "./components/SignUp";
import BlogDetail from "./components/BlogDetail";
import Navbar from "./components/Navbar";
import { useState, useEffect } from "react";

function App() {
  const [loginStatus, setLoginStatus] = useState(false);
  const [username, setUserName] = useState("");

  useEffect(() => {
    fetch(`https://${process.env.REACT_APP_BBS_BACKEND}/user/refresh`, {
      credentials: "include",
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.status === "success") {
          setUserName(res.username);
          setLoginStatus(true);
        } else {
          console.log("No cookies!");
        }
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <Navbar
              loginStatus={loginStatus}
              setLoginStatus={setLoginStatus}
              username={username}
            />
          }
        >
          <Route index element={<Home />} />
          <Route
            exact
            path="/create"
            element={<Create loginStatus={loginStatus} />}
          />
          <Route exact path="/blog/:id" element={<BlogDetail />} />
        </Route>
        <Route
          exact
          path="/login"
          element={
            <Login
              loginStatus={loginStatus}
              setLoginStatus={setLoginStatus}
              setUserName={setUserName}
            />
          }
        />
        <Route exact path="/signup" element={<SignUp />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
