// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getStorage } from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAJKHB7MgdVMuynmccUHNwHmzwxMX6qMtU",
  authDomain: "bbsblog-b3a42.firebaseapp.com",
  projectId: "bbsblog-b3a42",
  storageBucket: "bbsblog-b3a42.appspot.com",
  messagingSenderId: "556486279914",
  appId: "1:556486279914:web:c6cedf4208874b65648d93",
  measurementId: "G-BJ0HN3Q7DR",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const storage = getStorage(app);
