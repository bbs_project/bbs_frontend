import React, { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
const SignUp = () => {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [validatePass, setPasswordValidate] = useState("");
  const [error, setError] = useState({});
  const navigate = useNavigate();

  useEffect(() => {
    if (error.invalid) {
      alert(error.invalid);
    }
  }, [error]);

  const handleSignUp = async (e) => {
    e.preventDefault();
    if (password === validatePass) {
      const res = await fetch(
        `https://${process.env.REACT_APP_BBS_BACKEND}/user/signup`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            username,
            email,
            password,
          }),
        }
      );

      let data;
      if (res.status === 200) {
        alert("Sign up successfully");
        navigate("/login", { replace: true });
      } else {
        data = await res.json();
        // setError(res);
      }
      setError(data);
    } else if (password.length !== 0 && validatePass.length === 0) {
      alert("You need to confirm password");
    } else {
      alert("Password miss match");
    }
  };

  return (
    <div className="signup">
      <h2>Sign Up</h2>
      <form onSubmit={handleSignUp}>
        <label>Username</label>
        <input
          maxLength={50}
          type="text"
          onChange={(e) => setUsername(e.target.value)}
        />
        {error.username ? (
          <p className="text-danger">{error.username}</p>
        ) : null}
        <label>Password</label>
        <input
          maxLength={20}
          type="password"
          onChange={(e) => setPassword(e.target.value)}
        />
        {error.password ? (
          <p className="text-danger">{error.password}</p>
        ) : null}

        <label>Confirm Password</label>
        <input
          type="password"
          onChange={(e) => setPasswordValidate(e.target.value)}
        />
        <label>Email</label>
        <input type="text" onChange={(e) => setEmail(e.target.value)} />
        {error.email ? <p className="text-danger">{error.email}</p> : null}
        <br />
        <button>Sign Up</button>
      </form>
      <br />
      <br />
      <Link to="/">Back</Link>
    </div>
  );
};

export default SignUp;
