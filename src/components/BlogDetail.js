import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { CSVLink } from "react-csv";

export default function BlogDetail() {
  const { id } = useParams();

  const [blog, setBlog] = useState("");

  useEffect(() => {
    fetch(`https://${process.env.REACT_APP_BBS_BACKEND}/post/${id}`)
      .then((res) => res.json())
      .then((data) => {
        setBlog(data);
      })
      .catch((err) => console.log(err));
  }, [id]);

  return (
    <div className="home">
      <div className="blog-list">
        <div className="blog-preview">
          <div className="">
            <h2>{blog.title}</h2>
            <br></br>
            <div className="blog-info">
              <div style={{ paddingRight: "6em" }}>
                {" "}
                Author:
                <p>{blog.author_name}</p>
              </div>
              <div style={{ paddingRight: "6em" }}>
                Created On:
                <p>{blog.created_on}</p>
              </div>
              <div>
                Updated On: <p>{blog.updated_on}</p>
              </div>
            </div>
            <br></br>
            <div>
              <p>{blog.content}</p>
            </div>
            <br></br>
            <CSVLink
              data={[
                ["author_name", "title", "created_on", "updated_on"],
                [
                  blog.author_name,
                  blog.title,
                  blog.created_on,
                  blog.updated_on,
                ],
              ]}
            >
              Export
            </CSVLink>
          </div>
        </div>
      </div>
    </div>
  );
}
