import React, { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import Blog from "./Blog";

const BlogList = () => {
  const [blogs, setBlogs] = useState([]);
  const [pages, setPages] = useState(0);
  const handlePageClick = (e) => {
    fetch(
      `https://${process.env.REACT_APP_BBS_BACKEND}/post/list/${e.selected + 1}`
    )
      .then((res) => res.json())
      .then((data) => {
        setBlogs(data.data);
        window.scroll(0, 0);
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    fetch(`https://${process.env.REACT_APP_BBS_BACKEND}/post/list/1`)
      .then((res) => res.json())
      .then((data) => {
        setPages(data.count);
        setBlogs(data.data);
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <>
      <div className="blog-list">
        {blogs.map((blog, index) => (
          <div key={index}>
            <Blog blog={blog} />
          </div>
        ))}
      </div>
      <ReactPaginate
        className="pagination"
        nextLabel=">"
        onPageChange={handlePageClick}
        pageRangeDisplayed={5}
        pageCount={pages}
        previousLabel="<"
      />
    </>
  );
};

export default BlogList;
