import { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import {
  ref,
  uploadBytes,
  getDownloadURL,
  deleteObject,
} from "firebase/storage";
import { storage } from "../firebase/firebase";
import { v4 } from "uuid";

const Create = ({ loginStatus }) => {
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [error, setError] = useState({});

  // const [currentUser, setCurrentUser] = useState("");
  const navigate = useNavigate("");

  //Render
  useEffect(() => {
    console.log(loginStatus);
    if (!loginStatus) {
      navigate("/", { replace: true });
    }
    if (error.invalid) {
      alert(error.invalid);
    } //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [error, loginStatus]);

  //Firebase
  const [imageUpload, setImageUpload] = useState();

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (imageUpload == null) return;
    const snapshot = await uploadBytes(
      ref(storage, `images/${imageUpload.name + v4()}`),
      imageUpload
    );

    const thumbnail = await getDownloadURL(snapshot.ref);

    const result = await fetch(
      `https://${process.env.REACT_APP_BBS_BACKEND}/post/create`,
      {
        credentials: "include",
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          title,
          content,
          thumbnail,
        }),
      }
    );

    const thumbnailData = ref(storage, thumbnail);
    const data = await result.json();

    if (result.status === 200) {
      alert(data.message);
      navigate("/", { replace: true });
    } else if (result.status === 403) {
      alert("Failed to create post! Reason: " + data.error);
      deleteObject(thumbnailData);
      setError(data);
    }
  };

  const handleUploadImage = (e) => {
    const file = e.target.files[0];

    file.preview = URL.createObjectURL(file);

    setImageUpload(file);
  };

  return (
    <div className="create">
      <h2>Add a New Blog</h2>
      <form onSubmit={handleSubmit}>
        <label>Blog title:</label>
        <input
          maxLength={150}
          type="text"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
        {error.title ? <p className="text-danger">{error.title}</p> : null}

        <label>Blog content:</label>
        <textarea
          value={content}
          onChange={(e) => setContent(e.target.value)}
        ></textarea>
        {error.content ? <p className="text-danger">{error.content}</p> : null}
        {/* <label>Blog author:</label> */}
        {/* <input
          type="text"
          required
          value={currentUser}
          onChange={(e) => setCurrentUser(e.target.value)}
        /> */}
        {/* <select
          defaultValue={author}
          onChange={(e) => {
            console.log(e.target.value);
            setCurrentUser(e.target.value);
          }}
        >
          {author.map((user, id) => {
            return (
              <option key={id} value={user.id}>
                {user.username}
              </option>
            );
          })}
        </select> */}

        <br />
        <input type="file" onChange={handleUploadImage} />
        {/* <button onClick={uploadFile}> Upload Image</button> */}
        <br />
        <br />
        {imageUpload && (
          <img
            src={imageUpload.preview}
            alt="Preview thumbnail"
            width={400}
            height={300}
          />
        )}
        <br />
        <br />
        <button>Add Blog</button>
      </form>
      <br />
      <Link to="/">Back</Link>
      <br />
      <br />
    </div>
  );
};

export default Create;
