import React from "react";
import { Link } from "react-router-dom";
import { CSVLink } from "react-csv";

export default function Blog({ blog }) {
  return (
    <div className="blog-preview">
      <div style={{ marginRight: "16px" }}>
        <img src={blog.thumbnail} alt="Thumbnail" width={150} height={150} />
      </div>
      <div className="">
        <h2>{blog.title}</h2>
        <br></br>
        <div className="blog-info">
          <div style={{ paddingRight: "6em" }}>
            Author:
            <p>{blog.author_name}</p>
          </div>
          <div style={{ paddingRight: "6em" }}>
            Created On:
            <p>{blog.created_on}</p>
          </div>
          <div>
            Updated On: <p>{blog.updated_on}</p>
          </div>
        </div>
        <br></br>
        <p>
          {blog.content.length > 75
            ? blog.content.slice(0, 75) + "..."
            : blog.content}
        </p>
        <br></br>
        <div className="">
          <Link to={`blog/${blog.id}`}>Learn More</Link>
          &nbsp; &nbsp;&nbsp;
          <CSVLink
            data={[
              [blog.author_name, blog.title, blog.created_on, blog.updated_on],
            ]}
          >
            Export
          </CSVLink>
        </div>
        <br></br>
      </div>
    </div>
  );
}
