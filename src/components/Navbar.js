import { Outlet, Link, useNavigate } from "react-router-dom";

function Navbar({ loginStatus, setLoginStatus, username }) {
  //eslint-disable-next-line

  const navigate = useNavigate("");

  const handleLogout = () => {
    fetch(`https://${process.env.REACT_APP_BBS_BACKEND}/user/logout`, {
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    }).then((res) => {
      if (res.status === 200) {
        setLoginStatus(!loginStatus);
        navigate("/", { replace: true });
      }
    });
  };

  return !loginStatus ? (
    <>
      <nav className="navbar">
        <Link to="/">
          <h2>BBS</h2>
        </Link>
        <div className="links">
          <Link to="/">Home</Link>
          <Link to="/login">Login</Link>
          <Link to="/signup">Sign Up</Link>
        </div>
      </nav>
      <Outlet />
    </>
  ) : (
    <>
      <nav className="navbar">
        <Link to="/">
          <h2>BBS</h2>
        </Link>
        <div className="links">
          <Link to="/">Home</Link>
          <Link to="/create">New Blog</Link>
          <br />
          <br />
          <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hello&nbsp;&nbsp;&nbsp;
            {username}
          </p>
          <br />
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <button onClick={handleLogout}>Log out </button>
        </div>
      </nav>
      <Outlet />
    </>
  );
}

export default Navbar;
