import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";

function Login({ loginStatus, setLoginStatus, setUserName }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState({});
  // const [state, dispatch] = useContext(Context);

  const navigate = useNavigate();

  useEffect(() => {
    if (loginStatus) {
      navigate("/", { replace: true });
    }
    if (error.invalid) {
      alert(error.invalid);
    } //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [error, loginStatus]);

  const handleLogin = async (e) => {
    e.preventDefault();
    const res = await fetch(
      `https://${process.env.REACT_APP_BBS_BACKEND}/user/login`,
      {
        credentials: "include",
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email,
          password,
        }),
      }
    );
    const data = await res.json();

    if (res.status === 200) {
      alert("Login successfully");
      setLoginStatus(true);
      setUserName(data.data);
      navigate("/", { replace: true });
    } else {
      setError(data);
    }
  };

  return (
    <div className="create">
      <form onSubmit={handleLogin}>
        <h2>Login</h2>
        <label>Email</label>
        <input type="text" onChange={(e) => setEmail(e.target.value)} />
        {error.email ? <p className="text-danger">{error.email}</p> : null}
        <label>Password</label>
        <input type="password" onChange={(e) => setPassword(e.target.value)} />
        {error.password ? (
          <p className="text-danger">{error.password}</p>
        ) : null}
        <br />
        <button>Login</button>
        <br />
        <br />
      </form>
      <Link to="/">Back</Link>
    </div>
  );
}

export default Login;
